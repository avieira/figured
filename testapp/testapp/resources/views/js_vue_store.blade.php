<script type="text/javascript"> 
    let postsRepo = [
        @foreach($posts as $post)
                {
                    id: @json($post->id),
                    title:@json($post->title),
                    featured_image: @json($post->featured_image),
                    excerpt: @json($post->excerpt),
                    publish_by: @json(Carbon\Carbon::parse($post->publish_by)->diffForHumans() )
                },
        
        @endforeach
    ];

   

const store = new Vuex.Store({
  state: {

    posts: postsRepo
  }
})


  
</script>