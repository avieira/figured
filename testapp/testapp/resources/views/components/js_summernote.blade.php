<script type="text/javascript"> 

    $(document).ready(function() {
        $('#{{$elementId}}').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            placeholder: 'write some content here...'
        });
    });
  
</script>