<script type="text/javascript"> 

    $(document).ready(function() {
        $('.{{$elementClass}} input').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true,
        });
    });
  
</script>