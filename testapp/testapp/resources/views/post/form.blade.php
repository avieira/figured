<fieldset>                              
    <div class="form-group row text-lg">
        <div class="col-lg-12">
        {{ Form::label('title', 'Title of post',['class'=>'form-control-label'])}}
        {{ Form::text('title', null, array_merge(['class' => 'form-control validate[required]', 'placeholder'=>'please enter a title'])) }}
        </div>
    </div>
    <div class="form-group row text-lg">
        <div class="col-lg-12">
        {{ Form::label('content', 'Content',['class'=>'form-control-label form-group-horizontal'])}}
        {{ Form::textarea('content', null, array_merge(['class' => 'form-control', 'placeholder'=>'Description/Note'])) }}
        </div>
    </div>
    <div class="form-group row text-lg">
        <div class="col-lg-12">
        {{ Form::label('excerpt', 'Excerpt',['class'=>'form-control-label form-group-horizontal'])}}
        {{ Form::textarea('excerpt', null, array_merge(['class' => 'form-control', 'placeholder'=>'Type your excerpt here'])) }}
        </div>
    </div>
    <div class="form-group row text-lg">
        <div class="col-lg-12">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="featured_image" name="featured_image">
                <label class="custom-file-label" for="featured_image">Browse for a featured image</label>
            </div>
        </div>
        
    </div>
    <div class="form-group row text-lg">
        <div class="col-lg-12">
            {{ Form::label('publish_by', 'Publish By',['class'=>'form-control-label'])}}
            <div class="input-group publish_by_date  margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                {{ Form::text('publish_by',isset($post) ? null : Carbon\Carbon::today()->format('Y-m-d'), array_merge(['class' => 'form-control validate[required]'])) }}
            </div>
            @if($submit_button_text == 'Save') 
                @if(!empty($post->featured_image))
                    <p class="text-center">
                            <img src="{{$post->featured_image}}" class="py-4 preview-image" style="max-width:250px" /> </br>
                            <a href="#" class="py-4" id="remove_image">remove image</a>
                    </p>
                    {{ Form::hidden('remove_image', 'false')}}
                @else 
                    {{ Form::hidden('remove_image', 'false')}}
                @endif
            @endif
        </div>
    </div>
    <div class="form-group row text-lg">
        <div class="col-lg-12">
        {{ Form::button("<i class='fa fa-plus'></i> ".$submit_button_text,['type'=>'submit', 'class' => 'btn btn-large btn-primary ']) }}
        </div>
    </div>
</fieldset>


{{-- JavaScript Head Tag --}}
@section('javascript')
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    @parent
@stop

{{-- CSS Head Tag --}}
@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    
    <style>
        .note-popover.popover.in{
            display: none;
        }
        .custom-file-control:before{
        content: "Browse";
        }
        .custom-file-control:after{
        content: "Add files..";
        }
    </style>
    @parent
@stop

@section('footer_scripts')
@parent
    <script type="text/javascript"> 
        $(document).ready(function() {
            $("#remove_image").on('click',function(){
                $("input[name='remove_image']").val('true');
                $(this).hide();
                $(".preview-image").hide();
                
            });
        });
       
    </script>
   @include('post.js_validation')
   @include('components.js_summernote',["elementId"=>"content"])
   @include('components.js_datepicker',["elementClass"=>"publish_by_date"])
   

@stop