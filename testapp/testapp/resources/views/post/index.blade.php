@extends('layouts.app')

{{-- Page title --}}
@section('title')
    Posts
    @parent
@stop

@section('content')
<div class="container-fluid">
    <header class="head">
        <div class="main-bar row">
            <div class="col-lg-6 col-sm-4">
                <h4 class="nav_top_align">
                    <i class="fa fa-file-text"></i>
                    Manage Posts
                </h4>
            </div>
            <div class="col-lg-6 col-sm-8">
                <ol class="breadcrumb float-xs-right nav_breadcrumb_top_align">
                    <li class="breadcrumb-item active">Post List</li>
                </ol>
            </div>
        </div>
    </header>
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">List Posts</div>

                <div class="card-body">
                    <a href="{{ route('post.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Post
                    </a>
                    <div class="table-responsive py-4">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>
                                    <th> Title</th>
                                    <th class="hidden-xs">Excerpt</th>
                                    <th class="hidden-xs">Publish Date</th>
                                    <th style="width:300px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                <tr>
                                    <td class="highlight">
                                        <span class="success"></span>
                                        {{ $post->title }}
                                    </td>
                                    <td class="hidden-xs">{{ str_limit($post->excerpt, 20, '...') }}</td>
                                    <td class="hidden-xs">{{ $post->publish_by }}</td>
                                    <td>
                                        <a href="{{ route('post.show',$post->id) }}" class="btn btn-success btn-xs">
                                            <i class="fa fa-eye"></i> Preview
                                        </a>
                                        <a href="{{ route('post.edit',$post->id) }}" class="btn btn-warning btn-xs">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['post.destroy', $post->id],'style'=>'display:inline']) !!}
                                        <button type="submit" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                        
                                        {!! Form::close() !!}
                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $posts->links('vendor.pagination.bootstrap-4') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- JavaScript Head Tag --}}
@section('javascript')
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @parent
@stop

@section('footer_scripts')
@parent
<script type="text/javascript"> 
    $(document).ready(function() {
        $('.btn-danger').on('click', function () {
            var form = $(this).parents("form");
            swal({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this!',
                dangerMode: true,
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then((value) => {
                if(value){
                    form.submit();
                }
            });
            return false;
        });
     });
</script>
@stop

