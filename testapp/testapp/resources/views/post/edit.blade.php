@extends('layouts.app')

{{-- Page title --}}
@section('title')
    Post | Edit
    @parent
@stop

{{-- Page content --}}
@section('content')
<div class="container-fluid">
    <header class="head">
        <div class="main-bar row">
            <div class="col-lg-6 col-sm-4">
                <h4 class="nav_top_align">
                    <i class="fa fa-file-text"></i>
                    Manage Posts
                </h4>
            </div>
            <div class="col-lg-6 col-sm-8">
                <ol class="breadcrumb float-xs-right nav_breadcrumb_top_align">
                    <li class="breadcrumb-item">
                        <a href="{{ url('post') }}">Post List</a>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </div>
        </div>
    </header>
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Create a Post</div>

                <div class="card-body">
                        {!! Form::model($post,['method'=>'PATCH','route' => ['post.update',$post->id], 'class'=> 'form-horizontal', 'id'=>'post_form','files' => true]) !!}
                                
                            @include("post.form",["submit_button_text"=>"Save"])
                        
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
        
@endsection
