@extends('layouts.app')

{{-- Page title --}}
@section('title')
    Post | {{$post->title}}
    @parent
@stop

{{-- Page content --}}
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h1>{{$post->title}}</h1></div>
    
                    <div class="card-body">
                            @if(!empty($post->featured_image))
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="text-center">
                                            <img style="width:100%" src="{{$post->featured_image}}" class="py-4 preview-image"  />
                                    </p>
                                    <hr/>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-12">
                                    {!!$post->content!!}
                                </div>
                            </div>
                            <div class="row py-4">
                                <div class="col-lg-12">
                                    <h5>Excerpt</h5>
                                    {{$post->excerpt}}
                                </div>
                            </div>
                            <div class="row py-4">
                                <div class="col-lg-12">
                                    Published: {{  Carbon\Carbon::parse($post->publish_by)->diffForHumans() }}
                                </div>
                            </div>
                    </div>
                </div>
                
            </div>
        </div>
</div>
@stop


