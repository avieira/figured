<script type="text/javascript"> 
$(document).ready(function() {
    $('#post_form').bootstrapValidator({
        framework: 'bootstrap',
        
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: {
                        message: 'A title is required'
                    }
                }
            },
            content: {
                validators: {
                    notEmpty: {
                        message: 'Content for the blog post is required'
                    }
                }
            }
            
        }
    });
    
    
    
    
    });
    
  
</script>
<style>
    .help-block {
    color: red;
    }

    .form-group.has-error.has-danger .form-control-label {
    color: red;
    }

    .form-group.has-error.has-danger .form-control {
    border: 1px solid red;
    box-shadow: 0 0 0 0.2rem rgba(250, 16, 0, 0.18);
    }
</style>