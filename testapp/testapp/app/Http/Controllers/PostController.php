<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostRepo;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $post;

    public function __construct(PostRepo $post)
    {
        $this->post = $post;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = $this->post->paginate('publish_by');

        return view('post.index', compact('posts'))->with('i', ($request->input('page', 1) - 1) * 100);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'title' => 'required',
                'content' => 'required',
                'publish_by' => 'required|date',
                'featured_image' => 'image|max:2000',
                
        ]);
        
        $post =
        [
            "title" => $request->title,
            "content" => $request->content,
            "excerpt" => $request->excerpt,
            "publish_by" => $request->publish_by,
            "user_id" => Auth::id(),
        ];

        if ($request->hasFile('featured_image')) {
            $imgPath = $request->featured_image->path();
            $imgData = file_get_contents($imgPath);
            $type = pathinfo($imgPath, PATHINFO_EXTENSION);
            
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imgData);

            $post["featured_image"] = $base64;
        }

        $this->post->create($post);
        
        return redirect()->route('post')->with('success', 'Post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post->find($id);

        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->find($id);

        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'publish_by' => 'required|date',
            'featured_image' => 'image|max:2000',
            
        ]);
    
        $post =
        [
            "title" => $request->title,
            "content" => $request->content,
            "excerpt" => $request->excerpt,
            "publish_by" => $request->publish_by,
            "user_id" => Auth::id(),
        ];

        
        if ($request->hasFile('featured_image')) {
            $imgPath = $request->featured_image->path();
            $imgData = file_get_contents($imgPath);
            $type = pathinfo($imgPath, PATHINFO_EXTENSION);
            
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imgData);

            $post["featured_image"] = $base64;
        }

        if ($request->remove_image == "true") {
            $post["featured_image"] = "";
        }
        
        $this->post->update($post, $id);
        
        return redirect()->route('post')->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->post->delete($id);

        return redirect()->route('post')->with('success', 'Post removed successfully');
    }
}
