<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Repositories\PostRepo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $post;

    public function __construct(PostRepo $post)
    {
        $this->post = $post;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHomePage(Request $request)
    {
        $today = new DateTime(date("Y-m-d"));
        $posts = $this->post->all('publish_by')->where('publish_by', '<=', $today)->all();

        return view('home', compact('posts'));
    }
}
