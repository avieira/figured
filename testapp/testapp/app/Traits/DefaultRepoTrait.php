<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

//use Jenssegers\Mongodb\DB;

trait DefaultRepoTrait
{
    
    /**
     * @param array $columns
     * @return mixed
     */
    public function all($orderBy = 'id')
    {
        return $this->model->orderBy($orderBy, 'DESC')->get();
    }
   
    /**
      * @param int $perPage
      * @param array $columns
      * @return mixed
      */
    public function paginate($orderBy = 'id', $perPage = 50, $columns = array('*'))
    {
        return $this->model->orderBy($orderBy, 'DESC')->paginate($perPage, $columns);
    }
   
    /**
      * @param array $data
      * @return mixed
      */
    public function create(array $data)
    {
        return $this->model = $this->model->create($data);
    }
   
    /**
      * @param array $data
      * @param $id
      * @param string $attribute
      * @return mixed
      */
    public function update(array $data, $id, $attribute = "_id")
    {
        return DB::connection('mongodb')->collection($this->model->getCollection())
                    ->where($attribute, $id)
                    ->update($data, ['upsert' => false]);
    }
   
    /**
      * @param $id
      * @return mixed
      */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }
   
    /**
      * @param $id
      * @param array $columns
      * @return mixed
      */
    public function find($id)
    {
        $this->model = $this->model->findOrFail($id);
        return $this->model;
    }
   
    /**
      * @param $attribute
      * @param $value
      * @param array $columns
      * @return mixed
      */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, $value)->first($columns);
    }
   
    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function whereNotIn($attribute, $value, $columns = array('*'))
    {
        return $this->model->whereNotIn($attribute, $value)->select($columns)->get();
    }
}
