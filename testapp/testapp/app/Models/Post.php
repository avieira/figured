<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Post extends Eloquent
{
    use SoftDeletes;

    protected $collection = 'post_collection';
    protected $connection = 'mongodb';
    protected $dates = ['deleted_at','publish_by'];

    protected $fillable = [
        'title', 'content','excerpt','featured_image','publish_by','user_id'
    ];

    public function getCollection()
    {
        return $this->collection;
    }
}
