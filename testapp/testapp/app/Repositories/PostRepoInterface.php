<?php

namespace App\Repositories;

interface PostRepoInterface
{
    public function all($columns = array('*'));
   
    public function paginate($orderBy = 'id', $perPage = 50, $columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id, $attribute = "id");

    public function delete($id);

    public function find($id);

    public function findBy($attribute, $value, $columns = array('*'));
}
