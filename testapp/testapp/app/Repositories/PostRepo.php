<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\PostRepoInterface;
use App\Traits\DefaultRepoTrait;

class PostRepo implements PostRepoInterface
{
    use DefaultRepoTrait;

    private $model;

    public function __construct($model = null)
    {
        $this->model = $model ?: new Post;
    }
}
