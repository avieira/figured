# Figured - Blog Test

This is a small blogging engine using mongodb 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Docker can be used by navigating to the laradock folder and running the following commands:

```

sudo docker-compose up -d nginx mysql mongo

```
There is a guide available on laradock to install and docker etc http://laradock.io/getting-started/

### Installing

Once docker containers are up and running navigate to the laradock folder and run the following commands
```
sudo docker-compose exec --user=laradock workspace bash
```

And then once you have entered the bash you can run

```
composer install
```

Once all the dependencies are installed you can then update the database config, please note both mysql and mongodb configs need to be present in config/database

```
'default' => env('DB_CONNECTION', 'mysql'),

'mongodb' => [
            'driver'   => 'mongodb',
            'host'     => 'mongo',
            'port'     => '27017',
            'database' => '<database>',
            'username' => '',
            'password' => '',
            'options'  => [
                'database' => '',
            ]
        ],
```

You can now run the migrate refresh with seed to install the default user required for login as well as some test posts

```
php artisan migrate:refresh --seed
```

You can now login to edit and create posts with the following details

```
Username/Email: admin@admin.com

Password: password
```


Thanks
